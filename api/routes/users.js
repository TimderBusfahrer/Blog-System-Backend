const express = require('express');
const router = express.Router();
const checkAuth = require('../authentication/check-auth');

// Importing all the Controllers
const UserController = require('../controller/user.controller');

// Handle incoming POST requests from /user/signup
router.post('/signup', UserController.signupUser);

// Handle incoming POST requests from /user/login
router.post('/login', UserController.loginUser);

// Handle incoming DELETE requests from /user/'userId'
router.delete('/:userId', checkAuth, UserController.deleteUser);

module.exports = router;