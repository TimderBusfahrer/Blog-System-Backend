const express = require('express');
const router = express.Router();
const checkAuth = require('../authentication/check-auth');

// Importing all the Controllers
const ArticleController = require('../controller/articles.controller');

// Handle incoming GET requests from /articles
router.get('/', ArticleController.getArticles);

// Handle incoming POST requests from /articles
router.post('/', checkAuth, ArticleController.createArticle);

// Handle incoming GET requests from /articles/'acticleId'
router.get('/:articleId', ArticleController.getArticleById);

// Handle incoming PATCH requests from /articles/'acticleID'
router.patch('/:articleId', checkAuth, ArticleController.updateArticle);

// Handle incoming DELETE requests from /articles/'acticleID'
router.delete('/:articleId', checkAuth, ArticleController.deleteArticle);

module.exports = router;