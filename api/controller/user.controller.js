// Import all the libraries
const mongoose = require('mongoose');
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken');

// Import the Model it self
const User = require('../models/user');

exports.signupUser = (req, res, next) => {
    User.find({username: req.body.username}).exec().
    then(user => {
        if(user.length >= 1) {
            return res.status(422).json({
                message: 'username is still existing'
            });
        } else {
            bcrypt.hash(req.body.password, 10, (err, hash) => {
                if (err) {
                    return res.status(500).json({
                        error: err
                    });
                } else {
                    const user = new User({
                        _id: new mongoose.Types.ObjectId,
                        username: req.body.username,
                        password: hash
                    });
        
                    user.save().
                        then(result => {
                            res.status(200).json({
                                message: 'User created'
                            });
                        }).catch(err => {
                            console.log(err);
        
                            res.status(500).json({
                                error: err
                            });
                        });
                }
            });
        }
    });
}

exports.loginUser = (req, res, next) => {
    User.find( {username: req.body.username} ).exec().
    then(user => {
        if(user.length < 1) {
            return res.status(401).json({
                message: 'User not found, user doesn\'t exist'
            });
        }

        bcrypt.compare(req.body.password, user[0].password, (err, result) => {
            if(err) {
                console.log(err);
        
                res.status(500).json({
                    error: err
                });
            }

            if(result) {
                const token = jwt.sign({
                    username: user[0].username,
                    userId: user[0]._id
                },
                process.env.JWT_KEY, 
                {
                    expiresIn: '1h'
                });
            
                return res.status(200).json({
                    message: 'Auth successfully',
                    token: token
                });
            }

            return res.status(401).json({
                message: 'Auth failed'
            });
        });
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
}

exports.deleteUser = (req, res, next) => {
    User.remove({ _id: req.params.userId }).exec().
    then(result => {
        res.status(200).json({
            message: 'User deleted'
        });
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
}