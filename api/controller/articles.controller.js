// Import all the libraries
const mongoose = require('mongoose');

// Import the Model it self
const Article = require('../models/article');

exports.getArticles = (req, res, next) => {
    Article.find().select('_id author timestamp heading text').exec().
    then(docs => {
        const response =
            docs.map(doc => {
                return {
                    author: doc.author,
                    timestamp: doc.timestamp,
                    heading: doc.heading,
                    text: doc.text,
                    _id: doc._id,
                    requestThisArticle: {
                        type: 'GET',
                        url: 'http://localhost:3000/articles/' + doc._id
                    }
                }
            });

        if(docs.length > 0) {
            res.status(200).json(response);
        } else {
            res.status(404).json({
                message: "No articles found in the database"
            });
        }
        
    })
    .catch(err => {
        console.log(err);

        res.status(500).json({
            error: err
        });
    });
}

exports.createArticle = (req, res, next) => {
    const article = new Article({
        _id: new mongoose.Types.ObjectId(),
        author: req.body.author,
        timestamp: req.body.timestamp,
        heading: req.body.heading,
        text: req.body.text
    });

    article.save().then(result => {
        res.status(201).json({
            message: 'Created article successfully',
            createdArticles: {
                author: result.author,
                timestamp: result.timestamp,
                heading: result.heading,
                text: result.text,
                _id: result._id,
                request: {
                    type: 'GET',
                    url: 'http://localhost:3000/articles/' + result._id
                }
            }
        })
    }).catch(err => {
        console.log(err);

        res.status(500).json({
            error: err
        });
    });
}

exports.getArticleById = (req, res, next) => {
    const id = req.params.articleId;

    Article.findById(id).exec().
    then(doc => {
        if(doc) {
            res.status(200).json({
                author: doc.author,
                timestamp: doc.timestamp,
                heading: doc.heading,
                text: doc.text,
                _id: doc._id,
                requestAllAcricles: {
                    type: 'GET',
                    url: 'http://localhost:3000/articles/'
                }
            });
        } else {
            res.status(404).json({
                message: 'No valid entry fiund for provided articleId',
                enteredId: id
            });
        }   
    }).catch(err => {
        console.log(err);
        
        res.status(500).json({error: err});
    });
}

exports.updateArticle = (req, res, next) => {
    const id = req.params.articleId;

    const updateOps = {};
    for(const ops of req.body) {
        updateOps[ops.propName] = ops.value;
    }

    Article.update({_id: id}, { $set: updateOps }).exec().
    then(result => {
        res.status(200).json({
            message: 'Article updated with id: ' + id,
            request: {
                type: 'GET',
                url: 'http://localhost:3000/articles/' + id
            }
        });
    }).catch(err => {
        console.log(err);

        res.status(500).json({
            error: err
        });      
    });
}

exports.deleteArticle = (req, res, next) => {
    const id = req.params.articleId;
    Article.remove({_id: id}).exec().
    then(result => {
        res.status(200).json({
            message: 'Article deleted',
            request: {
                type: 'GET',
                url: 'http://localhost:3000/articles/'
            }
        });
    })
    .catch(err => {
        console.log(err);

        res.status(500).json({
            error: err
        });
    });
}