const mongoose = require('mongoose');

const articleSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    author: { type: String, required: true },
    timestamp: { type: String, required: true },
    heading: { type: String, required: true },
    text: { type: String, required: true }
});

module.exports = mongoose.model('Article', articleSchema);