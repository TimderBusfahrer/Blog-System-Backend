const express = require('express');
const app = express();

const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const articlesRoutes = require('./api/routes/articles');
const userRoutes = require('./api/routes/users');

// Setup the MongoDB Connection
mongoose.connect('mongodb://' + encodeURIComponent(process.env.MONGO_USERNAME) + ':' + encodeURIComponent(process.env.MONGO_PASSWORD) + '@' + encodeURIComponent(process.env.MONGO_HOST) + ':' + encodeURIComponent(process.env.MONGO_PORT) + '/' + encodeURIComponent(process.env.MONGO_DATABASE) + '?retryWrites=true');

// Setup the Morgan System
app.use(morgan('dev'));

// Setup the boby-parser System
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', '*');
    res.header('Access-Control-Allow-Methods', '*');

    //if(req.method === 'OPTIONS') {
    //    res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, GET, DELETE');
    //    return res.status(200).json({});
    //}

    next();
});

// Defind which Route is used for which routeparts 
app.use('/articles', articlesRoutes);
app.use('/user', userRoutes);

// Setup Error Handling
app.use((res, req, next) => {
    const error = new Error('404 -> Not found');
    error.status = 404;
    next(error);
});

app.use((err, req, res, next) => {
    res.status(err.status || 500);
    res.json({
        error: {
            message: err.message
        }
    });
});

module.exports = app;